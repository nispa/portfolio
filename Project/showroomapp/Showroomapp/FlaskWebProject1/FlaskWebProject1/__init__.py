"""
The flask application package.
"""

from flask import Flask
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager

app = Flask(__name__)
JWT_SECRET_KEY = 'Huvle7WyeZ67TJgr7Jby03g9MB84TRlI'
app.config.from_object(__name__)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)

import FlaskWebProject1.views
