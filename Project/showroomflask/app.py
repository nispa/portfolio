from flask import Flask
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager

app = Flask(__name__)
#JWT_SECRET_KEY = 'Huvle7WyeZ67TJgr7Jby03g9MB84TRlI'
app.config.from_envvar('ENV_FILE_LOCATION')
app.config.from_object(__name__)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)

"""
Routes and views for the flask application.
"""
from datetime import datetime
from db.db import initialize_db
from db.models import Machine 
from flask_restful import Api
from resources.routes import initialize_routes


api = Api(app)

app.config['MONGODB_SETTINGS'] = {
    'host': 'mongodb://localhost/showroom'
}
initialize_db(app)

initialize_routes(api)

@app.route('/')
def hello_world():
    return "Hello World 23!"

