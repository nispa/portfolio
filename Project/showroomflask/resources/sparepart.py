from flask import Response, request
from db.models import Machine, Sparepart, User
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource
from findObject import *

class SparepartsApi(Resource):
    def get(self):
        spareparts = Sparepart.objects().to_json()
        return Response(spareparts, mimetype="application/json", status=200)
    
    @jwt_required()
    def post(self):
        user_id = get_jwt_identity()
        body = request.get_json()
        body['machine'] = Machine.objects.get(id=body["machine"])
        #user = User.objects.get(id=user_id)
        sparepart = Sparepart(**body)
        sparepart.save()
        #user.update(push__spareparts=sparepart)
        #user.save()
        id = sparepart.id
        return {'id': str(id)}, 200

class SparepartApi(Resource):
    def get(self, id):
        sparepart = Sparepart.objects.get(id=id).to_json()
        return Response(spareparts, mimetype="application/json", status=200)

    @jwt_required
    def put(self, id):
        user_id = get_jwt_identity()
        sparepart = Sparepart.objects.get(id=id)
        body = request.get_json()
        sparepart.update(**body)
        return '', 200

    @jwt_required
    def delete(self, id):
        user_id = get_jwt_identity()
        sparepart = Sparepart.objects.get(id=id, added_by=user_id)
        sparepart.delete()
        return '', 200

class SparepartByImageApi(Resource):
    def post(self):
        body = request.get_json()
        detections = StartDetection(body['image'], False)
        sparepartNames = []
        for detection in detections:
            sparepartNames.append(detection.get('name'))

        spareparts = Sparepart.objects(name__in=sparepartNames)
        for sparepartPred in spareparts:
            for prediction in detections:
                if sparepartPred.name == prediction.get('name'):
                    sparepartPred.update(set__prediction = int(prediction.get('score')))
 #                   sparepartPred.prediction = int(prediction.get('score'))
  #                  sparepart = Sparepart.objects.get(id=sparepartPred.id)
   #                 sparepart.update(**sparepartPred)

        sparepartsReturn = Sparepart.objects(name__in=sparepartNames).to_json()

        return Response(sparepartsReturn, mimetype="application/json", status=200)

